#!/bin/bash

if [ ! -f outputs/header.txt ] ; then
   cont="1"; 
   for elem in `cat outputs/data.txt | head -n 1` ; do
      t=`echo $elem | sed 's|-2.0||g' | sed 's|-0.0||g'`
      echo $cont $t;
      cont=`echo "$cont + 1"|bc`
   done > outputs/header.txt
fi

for elem in `cat all_codes.txt | awk '{print $3}' | uniq ` ; do
   codes="\$1" 
   for var in `cat all_codes.txt | grep " $elem " | awk '{print $1,$3}' | awk '{print $1}'` ; do 
      new_code=`cat outputs/header.txt | grep " $var$" | awk '{print $1}'`
      if [ ! "$new_code" == "" ] ;then
         codes="$codes,\$${new_code}"
      fi
   done ;
   #echo $elem $codes
   eval "cat outputs/data.txt | awk '{print $codes}' > outputs/ID_$elem.txt" 
done
