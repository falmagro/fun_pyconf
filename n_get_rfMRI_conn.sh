#!/bin/bash

#######################################
# Downloading rfMRI connectivity data #
#######################################

rm -rf rfMRI_conn
mkdir -p rfMRI_conn
for sub in "2" "3" ; do
   for elem in `cat s${sub}.txt `; do
      for pack in "32155" "32156" "32157" "32158" ; do
         # ukbfetch is a special script provided by UKB to
         # download bulk data. keys.key is the private
         # key for each project to download their data.
         # Availability of those 2 files is required.
         ./priv/ukbfetch -e$elem -d${pack}_${sub}_0 -a./priv/FMRIBkeys.key
         if [ -f ${elem}_${pack}_${sub}_0.txt ] ; then
	    mv ${elem}_${pack}_${sub}_0.txt rfMRI_conn/;
         fi
      done
   done
done



