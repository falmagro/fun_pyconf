#!/bin/bash

#######################################
# Downloading rfMRI connectivity data #
#######################################

#rm -rf rfMRI_conn
#mkdir -p rfMRI_conn
#for sub in "2" "3" ; do
#   for elem in `cat s${sub}.txt `; do
#      for pack in "32155" "32156" "32157" "32158" ; do
         # ukbfetch is a special script provided by UKB to
         # download bulk data. keys.key is the private
         # key for each project to download their data.
         # Availability of those 2 files is required.
#         ./priv/ukbfetch -e$elem -d${pack}_${sub}_0 -a./priv/keys.key 
#      done
#   done
#done


# Generate these 4 files from the previous steps
#   - rfMRI_d25_NodeAmplitudes_v1.txt
#   - rfMRI_d100_NodeAmplitudes_v1.txt
#   - rfMRI_d25_partialcorr_v1.txt
#   - rfMRI_d100_partialcorr_v1.txt
#
cp $orig_dir/rfMRI_d25_NodeAmplitudes_v1.txt  pyconf_inputs/
cp $orig_dir/rfMRI_d100_NodeAmplitudes_v1.txt pyconf_inputs/
cp $orig_dir/rfMRI_d25_partialcorr_v1.txt     pyconf_inputs/ 
cp $orig_dir/rfMRI_d100_partialcorr_v1.txt    pyconf_inputs/

