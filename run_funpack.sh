#!/bin/bash

. py_funpack/bin/activate

cat subj.txt | grep "^2" | awk -F "" '{print $2$3$4$5$6$7$8}' > s2.txt
cat subj.txt | grep "^3" | awk -F "" '{print $2$3$4$5$6$7$8}' >	s3.txt

rm -rf logs outputs ; 
mkdir logs outputs;

# For each "imaging visit"
for sub in "2" "3" ; do
   cat all_codes.txt | awk '{print $1}' > tmp_codes.txt

   echo "fmrib_unpack -s s$sub.txt -v tmp_codes.txt -vi $sub -nj -1 -tm NaN -ts \" \" -def outputs/desc_s${sub}.txt --log_file logs/s${sub}_log.txt -n -n -n -wl outputs/s${sub}_out.txt ./ukb675534.csv" >> logs/commands.txt
   fmrib_unpack \
         -s s$sub.txt \
         -v tmp_codes.txt \
         -vi $sub \
         -nj -1 \
         -tm NaN \
         -ts " " \
         --log_file logs/s${sub}_log.txt \
         -def outputs/desc_s${sub}.txt \
         -n -n -n -wl \
         outputs/s${sub}_out.txt \
         ./ukb675534.csv
done

rm tmp_codes.txt
cat outputs/s3_out.txt | tail -n +2 | awk '{print 3$0}' > outputs/tmp3
cat outputs/s2_out.txt | awk '{print 2$0}' > outputs/tmp2
cat outputs/tmp2 outputs/tmp3 | sort -n > outputs/data.txt &

