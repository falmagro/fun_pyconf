#!/bin/bash

rm -rf pyconf_inputs;
mkdir -p pyconf_inputs;

# This needs to be pre-generated or copied
cp excluded_subjects.txt subj.txt pyconf_inputs/

# Big files that do not need correction
cat outputs/ID_ASL_IDPs.txt | tail -n +2 > pyconf_inputs/ASL_IDPs.txt
cat outputs/ID_QSM_IDPs.txt | tail -n +2 > pyconf_inputs/QSM_IDPs.txt
cat outputs/ID_WMH_IDPs.txt | tail -n +2 > pyconf_inputs/ID_WMH.txt
cat outputs/ID_FS_IDPs.txt  | tail -n +2 > pyconf_inputs/FS_IDPs.txt

# Small files that do not need correction
cat outputs/ID_EDDYQC.txt           | tail -n +2 > pyconf_inputs/ID_EDDYQC.txt
cat outputs/ID_DVARS.txt            | tail -n +2 > pyconf_inputs/ID_DVARS.txt 
cat outputs/ID_FLIPPEDSWI.txt       | tail -n +2 > pyconf_inputs/ID_FLIPPEDSWI.txt 
cat outputs/ID_FST2.txt             | tail -n +2 > pyconf_inputs/ID_FST2.txt
cat outputs/ID_HEADMOTION.txt       | tail -n +2 > pyconf_inputs/ID_HEADMOTION.txt 
cat outputs/ID_HEADMOTIONST.txt     | tail -n +2 > pyconf_inputs/ID_HEADMOTIONST.txt
cat outputs/ID_HEADSIZE.txt         | tail -n +2 > pyconf_inputs/ID_HEADSIZE.txt
cat outputs/ID_NEWEDDY.txt          | tail -n +2 > pyconf_inputs/ID_NEWEDDY.txt
cat outputs/ID_SCALING.txt          | tail -n +2 > pyconf_inputs/ID_SCALING.txt
cat outputs/ID_SEX.txt              | tail -n +2 > pyconf_inputs/ID_SEX.txt
cat outputs/ID_SITE.txt             | tail -n +2 > pyconf_inputs/ID_SITE.txt
cat outputs/ID_STRUCTHEADMOTION.txt | tail -n +2 > pyconf_inputs/ID_STRUCTHEADMOTION.txt
cat outputs/ID_TABLEPOS.txt         | tail -n +2 > pyconf_inputs/ID_TABLEPOS.txt
cat outputs/ID_TE.txt               | tail -n +2 > pyconf_inputs/ID_TE.txt
 
# Small files that are not available yet in the showcase
#cat outputs/ID_BATCH.txt       | tail -n +2 > pyconf_inputs/ID_BATCH.txt
#cat outputs/ID_CMRR.txt        | tail -n +2 > pyconf_inputs/ID_CMRR.txt
#cat outputs/ID_SCANEVENTS.txt  | tail -n +2 > pyconf_inputs/ID_SCANEVENTS.txt 
#cat outputs/ID_PROTOCOL.txt    | tail -n +2 > pyconf_inputs/ID_PROTOCOL.txt
#cat outputs/ID_SERVICEPACK.txt | tail -n +2 > pyconf_inputs/ID_SERVICEPACK.txt
#cat outputs/ID_SCANEVENTS.txt  | tail -n +2 > pyconf_inputs/ID_SCANEVENTS.txt
#cat outputs/ID_TIMEPOINTS.txt  | tail -n +2 > pyconf_inputs/ID_TIMEPOINTS.txt
# Copy them from somewhere else
cat $orig_dir/ID_BATCH.txt       > pyconf_inputs/ID_BATCH.txt
cat $orig_dir/ID_CMRR.txt        > pyconf_inputs/ID_CMRR.txt
cat $orig_dir/ID_SCANEVENTS.txt  > pyconf_inputs/ID_SCANEVENTS.txt 
cat $orig_dir/ID_PROTOCOL.txt    > pyconf_inputs/ID_PROTOCOL.txt
cat $orig_dir/ID_SERVICEPACK.txt >  pyconf_inputs/ID_SERVICEPACK.txt
cat $orig_dir/ID_SCANEVENTS.txt  > pyconf_inputs/ID_SCANEVENTS.txt
cat $orig_dir/ID_TIMEPOINTS.txt  > pyconf_inputs/ID_TIMEPOINTS.txt

# rfMRI connectivity
./get_rfMRI_conn.sh


# IDPs
fil="outputs/ID_IDPs.txt"
cat $fil | awk '{print $893}' | sed 's|11025|1|g' | sed 's|11027|2|g' | sed 's|11026|3|g' |sed 's|11028|4|g'  > pyconf_inputs/tmp_2.txt
cat $fil | awk '{$893=""; print $0}' > pyconf_inputs/tmp_1.txt
paste -d " " pyconf_inputs/tmp_1.txt pyconf_inputs/tmp_2.txt | tail -n +2> pyconf_inputs/IDPs.txt
rm pyconf_inputs/tmp_*txt


# OTHER_GENERAL
fil="outputs/ID_OTHER_GENERAL.txt"
cat $fil | awk '{print $1,$2,$3,$4}' > pyconf_inputs/tmp_1.txt
cat $fil | awk '{print $5}' | sed 's|NaN|NaNTNaN|g'| awk -F "T" '{print $1}' > pyconf_inputs/tmp_2.txt
paste -d " " pyconf_inputs/tmp_1.txt pyconf_inputs/tmp_2.txt | tail -n +2> pyconf_inputs/ID_OTHER_GENERAL.txt
rm pyconf_inputs/tmp_*txt


# initial_workspace
fil="outputs/ID_initial_workspace.txt"
cat $fil | awk '{print $1}' > pyconf_inputs/tmp_1.txt
cat $fil | awk '{print $2}' | sed 's|-||g'> pyconf_inputs/tmp_2.txt
cat $fil | awk '{print $3}' | sed 's|NaN|NaNTNaN|g'| awk -F "T" '{print $2}' | sed 's|:||g'> pyconf_inputs/tmp_3.txt
yes NaN | head -n `cat pyconf_inputs/tmp_1.txt | wc -l` > pyconf_inputs/tmp_4.txt
cat $fil | awk '{print $4}' | sed 's|11025|1|g' | sed 's|11027|2|g' | sed 's|11026|3|g' |sed 's|11028|4|g'  > pyconf_inputs/tmp_5.txt
cat $fil | awk '{print $5}' > pyconf_inputs/tmp_6.txt 
paste -d " " pyconf_inputs/tmp_1.txt pyconf_inputs/tmp_2.txt pyconf_inputs/tmp_3.txt pyconf_inputs/tmp_4.txt pyconf_inputs/tmp_5.txt pyconf_inputs/tmp_6.txt | tail -n +2> pyconf_inputs/ID_initial_workspace.txt
rm pyconf_inputs/tmp_?.txt

